/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import Vuetify from 'vuetify';

Vue.use(Vuetify);

import 'material-design-icons-iconfont/dist/material-design-icons.css'

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('layout', require('./components/layouts.vue').default);
Vue.component('login', require('./components/Auth.vue').default);

// client
Vue.component('order-doors', require('./components/client/OrderDoors').default);
Vue.component('invoices', require('./components/client/Invoices').default);
Vue.component('cart', require('./components/client/Cart').default);
Vue.component('new-cart', require('./components/client/NewCart').default);
Vue.component('feronerie', require('./components/client/Feronerie').default);
Vue.component('elemente-toc', require('./components/client/ElementeToc').default);
Vue.component('pdf', require('./components/client/Pdf').default);
Vue.component('client-plinta', require('./components/client/Plinta').default);
Vue.component('offer-clients', require('./components/client/OfferClients').default);
Vue.component('offer-clients-pdf', require('./components/client/OfferClientsPdf').default);
Vue.component('offer-clients-pdf-from-order', require('./components/client/OfferClientsPdfFromOrder').default);


// operator
Vue.component('operator-product-edit', require('./components/operator/ProductEdit').default);
Vue.component('operator-parteners-edit', require('./components/operator/Parteners').default);
Vue.component('config-operator', require('./components/operator/Config').default);
Vue.component('invoices-operator', require('./components/operator/Invoices').default);
Vue.component('pdf-operator', require('./components/operator/PdfOperator').default);
Vue.component('group-order-details-operator', require('./components/operator/GroupOrderDetails').default);
Vue.component('group-manufacture-pdf-operator', require('./components/operator/GroupManufacturePdf').default);
Vue.component('add-new-door', require('./components/operator/AddNewDoor').default);
Vue.component('proforma-client', require('./components/operator/proformaClientInvoice').default);
Vue.component('proforma-client-new', require('./components/operator/proformaClientInvoiceNew').default);
Vue.component('extension-config', require('./components/operator/ExtensionConfig').default);
Vue.component('excel-export', require('./components/operator/Excel').default);
Vue.component('operator-colors', require('./components/operator/Colors').default);
Vue.component('operator-tags', require('./components/operator/Tags').default);
Vue.component('operator-series', require('./components/operator/Series').default);
Vue.component('operator-windows', require('./components/operator/Windows').default);
Vue.component('operator-moldings', require('./components/operator/Molding').default);
Vue.component('extensii-edit-product', require('./components/operator/ExtensiiEditProduct').default);


// ### MANERE ###
Vue.component('operator-minere', require('./components/operator/Minere').default);

// ### PLINTA ###
Vue.component('operator-plinta', require('./components/operator/Plinta').default);



Vue.component('oferta-cos', require('./components/pdf/OfertaCos').default);
Vue.component('oferta-usa', require('./components/pdf/OfertaUsa').default);
Vue.component('comp-el-i', require('./components/ui/EliUi').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Notifications from 'vue-notification';

Vue.use(Notifications);

import moment from 'moment';
Vue.prototype.$moment = moment;

import VueCountdownTimer from 'vuejs-countdown-timer'
Vue.use(VueCountdownTimer);


import VueExcelXlsx from "vue-excel-xlsx";
Vue.use(VueExcelXlsx);

import linq from 'linq';
// Vue.prototype.$linq = linq;

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    props: {
        source: null,
    },
    data: {
        drawer: null,
        overlay: true,
        mini: false,
    },



    mounted() {
        setTimeout(() => {
            this.overlay = false
        }, 500)
    }

});
