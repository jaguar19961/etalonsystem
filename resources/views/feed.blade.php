<!DOCTYPE html>
<html>
<head>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <title>Profildoors product feed</title>
    <style>
        .body {
            font-family: "Inter", sans-serif;
            background-color: #155bd5;
            height: 100vh;
            width: 100%;
            display: grid;
            grid-template-columns: 1fr 1fr;
            text-align: center;
            align-items: center;
        }

        .body .explication .dwn_btn{
            color: #155bd5;
            background-color: #fff;
            font-weight: bold;
            border: 1px solid transparent;
            padding: 12px 21px;
            text-decoration: none;
            border-radius: 4px;
            box-shadow: 0px 0px 1px rgba(40, 41, 61, 0.04), 0px 2px 4px rgba(96, 97, 112, 0.16);
            margin-top: 20px;
        }

        .body .explication .dwn_btn:hover{
            background-color: #cfddf5;
        }


        .body .explication {
            display: flex;
            flex-direction: column;
            align-items: baseline;
            padding: 100px;
            text-align: left;
            color: #fff;
        }

        .body .explication .logo {
            filter: invert(1);
        }

        .body .explication h1 {
            color: #fff;
            font-weight: 700;
            text-shadow: 0 3px 8px #00000017;

        }

        .body .img {
            width: 705px;
            height: 469px;
        }

        .body .img img {
            width: 100%;
            height: 100%;
            object-fit: contain;
            border-radius:20px;
        }

    </style>
</head>
<body>
<div class="body">
<div class="explication">
    <div class="logo">
        <img src="/images/auth/logo.png" alt="">
    </div>
    <h1>Corporation products feed</h1>
    <p>Access to our products updated 24/7. The fastest access method.</p>
    <a class="dwn_btn" href="/feed/download">Click to download your excel</a>
</div>
<div class="img">
    <img src="/images/bg_download.jpg" alt="">
</div>
</div>

</body>
</html>

