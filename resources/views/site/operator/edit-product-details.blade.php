@extends('site.layouts.app')
@section('content')
    <operator-product-edit :getdoors="{{json_encode($doorsSerie)}}"
                           :system_moldings="{{json_encode($system_moldings)}}"></operator-product-edit>
@endsection
