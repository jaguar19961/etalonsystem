
<v-navigation-drawer
    class="nav-distance"
    v-model="drawer"
    :mini-variant.sync="mini"
    permanent
{{--    dark--}}
    app
{{--    src="https://cdn.vuetifyjs.com/images/backgrounds/bg-2.jpg"--}}
>
    <v-list dense>
        <v-list-item class="px-2">
            <v-list-item-avatar>
                <v-img src="https://randomuser.me/api/portraits/men/85.jpg"></v-img>
            </v-list-item-avatar>

            <v-list-item-title>{{auth()->user()->name}}</v-list-item-title>

            <v-btn
                icon
                @click.stop="mini = !mini"
            >
                <v-icon>mdi-chevron-left</v-icon>
            </v-btn>
        </v-list-item>

        <v-divider></v-divider>
        <v-list-item link onclick="window.location.assign('/cabinet/client/contul-meu')">
            <v-list-item-action>
                <v-icon>mdi-home</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title >
                    <v-badge
                        color="pink"
                        content="In curind">
                        Dashboard
                    </v-badge>
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>
        <v-list-item link onclick="window.location.assign('/cabinet/client/cos')">
            <v-list-item-action>
                <v-icon>mdi-cart</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title onclick="window.location.assign('/cabinet/client/cos')">Cos</v-list-item-title>
            </v-list-item-content>
        </v-list-item>
        <v-list-item link onclick="window.location.assign('/cabinet/client/facturi')">
            <v-list-item-action>
                <v-icon>mdi-book</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title onclick="window.location.assign('/cabinet/client/facturi')">Facturi</v-list-item-title>
            </v-list-item-content>
        </v-list-item>

        <v-list-item link onclick="window.location.assign('/cabinet/client/comanda-noua')">
            <v-list-item-action>
                <v-icon>mdi-door</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title >Usi de interior</v-list-item-title>
            </v-list-item-content>
        </v-list-item>
        <v-list-item link onclick="window.location.assign('/cabinet/client/feronerie')">
            <v-list-item-action>
                <v-icon>mdi-outdoor-lamp</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title >Feronerie</v-list-item-title>
            </v-list-item-content>
        </v-list-item>

        <v-list-item link onclick="window.location.assign('/cabinet/client/elemente-toc')">
            <v-list-item-action>
                <v-icon>mdi-album</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title >Elemente toc</v-list-item-title>
            </v-list-item-content>
        </v-list-item>

{{--        <v-list-item link onclick="window.location.assign('/cabinet/client/oferte-clienti')">--}}
{{--            <v-list-item-action>--}}
{{--                <v-icon>mdi-tag</v-icon>--}}
{{--            </v-list-item-action>--}}
{{--            <v-list-item-content>--}}
{{--                <v-list-item-title >Oferte clienti</v-list-item-title>--}}
{{--            </v-list-item-content>--}}
{{--        </v-list-item>--}}


        <v-list-item link onclick="window.location.assign('/cabinet/client/plinta')">
            <v-list-item-action>
                <v-icon>mdi-fit-to-page-outline</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title >Plinta</v-list-item-title>
            </v-list-item-content>
        </v-list-item>

        <v-list-item link onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <v-list-item-action>
                <v-icon>mdi-logout</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title>
                    Iesire din cont
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>
    </v-list>
</v-navigation-drawer>
<v-app-bar app color="indigo" dark class="d-flex">
    <div>
{{--        <v-btn class="ma-2" fab color="teal" @click="drawer = !drawer">--}}
{{--            <v-icon>mdi-format-list-bulleted-square</v-icon>--}}
{{--        </v-btn>--}}
    </div>
{{--    <div>--}}
{{--        <v-btn class="mx-2" fab dark color="pink" @click.stop="doorMenu = !doorMenu">--}}
{{--            <v-icon dark>mdi-plus</v-icon>--}}
{{--        </v-btn>--}}
{{--    </div>--}}
</v-app-bar>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
