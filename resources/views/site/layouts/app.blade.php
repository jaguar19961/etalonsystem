<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profildoors Management system</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>

<div id="app" v-cloak>
{{--    <div class="v-overlay v-overlay--active theme--dark" style="z-index: 7;" v-if="overlay">--}}
{{--        <div class="v-overlay__scrim"--}}
{{--             style="opacity: 0.97; background-color: rgb(33, 33, 33); border-color: rgb(33, 33, 33);"></div>--}}
{{--        <div class="v-overlay__content">--}}
{{--            <div role="progressbar" aria-valuemin="0" aria-valuemax="100"--}}
{{--                 class="v-progress-circular v-progress-circular--indeterminate" style="height: 64px; width: 64px;">--}}
{{--                <svg xmlns="http://www.w3.org/2000/svg"--}}
{{--                     viewBox="21.333333333333332 21.333333333333332 42.666666666666664 42.666666666666664"--}}
{{--                     style="transform: rotate(0deg);">--}}
{{--                    <circle fill="transparent" cx="42.666666666666664" cy="42.666666666666664" r="20"--}}
{{--                            stroke-width="2.6666666666666665" stroke-dasharray="125.664"--}}
{{--                            stroke-dashoffset="125.66370614359172px" class="v-progress-circular__overlay"></circle>--}}
{{--                </svg>--}}
{{--                <div class="v-progress-circular__info"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <v-app id="inspire">

        @guest()
            <v-app-bar app color="indigo" dark>
                {{--            <v-app-bar-nav-icon @click.stop="drawer = !drawer" />--}}
                <v-toolbar-title class="d-flex justify-content-between">
                    <div>
                        <img class="logo-img" src="/images/auth/logo.png" alt=""> Profildoors Management system
                    </div>
                </v-toolbar-title>
            </v-app-bar>
        @else
            @if(\Illuminate\Support\Facades\Auth::user()->user_type == 1)
                @include('site.layouts.logged-menu')
            @elseif(\Illuminate\Support\Facades\Auth::user()->user_type == 2)
                @include('site.layouts.logged-menu-operator')
            @endif
        @endif


        <v-content>
            @yield('content')

        </v-content>

        <v-footer color="indigo" app>
            <span class="white--text"> Copyright &copy; 2019. Developed by MIDAVCO</span>
        </v-footer>
    </v-app>
</div>

<script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>



