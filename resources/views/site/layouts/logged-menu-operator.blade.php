<v-navigation-drawer
    class="nav-distance"
    v-model="drawer"
    :mini-variant.sync="mini"
    permanent
    {{--    dark--}}
    app
    {{--    src="https://cdn.vuetifyjs.com/images/backgrounds/bg-2.jpg"--}}
>
    <v-list dense>
        <v-list-item class="px-2">
            <v-list-item-avatar>
                <v-img src="https://randomuser.me/api/portraits/men/85.jpg"></v-img>
            </v-list-item-avatar>

            <v-list-item-title>{{auth()->user()->name}}</v-list-item-title>

            <v-btn
                icon
                @click.stop="mini = !mini"
            >
                <v-icon>mdi-chevron-left</v-icon>
            </v-btn>
        </v-list-item>

        <v-divider></v-divider>
        <v-list-item link>
            <v-list-item-action>
                <v-icon>mdi-home</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title onclick="window.location.assign('/cabinet/operator/contul-meu')">Dashboard
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>
        <v-list-item link>
            <v-list-item-action>
                <v-icon>mdi-book-plus</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title onclick="window.location.assign('/cabinet/operator/editare-produse')">Editare
                    produse
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>
        <v-list-item link>
            <v-list-item-action>
                <v-icon>mdi-account-group</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title onclick="window.location.assign('/cabinet/operator/parteneri')">Parteneri
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>

        <v-list-item link>
            <v-list-item-action>
                <v-icon>mdi-chart-line</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title onclick="window.location.assign('/cabinet/operator/facturi')">Facturi noi
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>

        <v-list-item link>
            <v-list-item-action>
                <v-icon>mdi-semantic-web</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title onclick="window.location.assign('/cabinet/operator/facturi-fabrica')">Facturi catre
                    fabrica
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>

        <v-list-item link>
            <v-list-item-action>
                <v-icon>mdi-cube-send</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title onclick="window.location.assign('/cabinet/operator/facturi-fabrica')">Comenzi spre
                    livrare
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>

        <v-list-group
            prepend-icon="mdi-settings-outline"
            no-action>
            <template v-slot:activator>
                <v-list-item-content>
                    <v-list-item-title>Editari</v-list-item-title>
                </v-list-item-content>
            </template>
            <v-list-item link>
                <v-list-item-action>
                    <v-icon>mdi-book-plus</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title onclick="window.location.assign('/cabinet/operator/editare-culori')">Editare
                        Culori
                    </v-list-item-title>
                </v-list-item-content>
            </v-list-item>
            <v-list-item link>
                <v-list-item-action>
                    <v-icon>mdi-book-plus</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title onclick="window.location.assign('/cabinet/operator/tags')">Editare
                        Taguri
                    </v-list-item-title>
                </v-list-item-content>
            </v-list-item>
            <v-list-item link>
                <v-list-item-action>
                    <v-icon>mdi-book-plus</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title onclick="window.location.assign('/cabinet/operator/series')">Editare
                        Serii
                    </v-list-item-title>
                </v-list-item-content>
            </v-list-item>
            <v-list-item link>
                <v-list-item-action>
                    <v-icon>mdi-book-plus</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title onclick="window.location.assign('/cabinet/operator/editare-molding')">Editare
                        Insertii
                    </v-list-item-title>
                </v-list-item-content>
            </v-list-item>
            <v-list-item link>
                <v-list-item-action>
                    <v-icon>mdi-book-plus</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title onclick="window.location.assign('/cabinet/operator/editare-insertii')">Editare
                        Sticla
                    </v-list-item-title>
                </v-list-item-content>
            </v-list-item>
            <v-list-item link>
                <v-list-item-action>
                    <v-icon>mdi-doorbell</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title onclick="window.location.assign('/cabinet/operator/minere')">Editare Minere
                    </v-list-item-title>
                </v-list-item-content>
            </v-list-item>
            <v-list-item link>
                <v-list-item-action>
                    <v-icon>mdi-content-duplicate</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title onclick="window.location.assign('/cabinet/operator/plinta')">Editare plinta
                    </v-list-item-title>
                </v-list-item-content>
            </v-list-item>
        </v-list-group>


        <v-list-group
            prepend-icon="mdi-settings-outline"
            no-action>
            <template v-slot:activator>
                <v-list-item-content>
                    <v-list-item-title>Settings</v-list-item-title>
                </v-list-item-content>

            </template>
            <v-list-item
                link
                onclick="window.location.assign('/cabinet/operator/configurari')">
                <v-list-item-action>
                    <v-icon>mdi-settings-outline</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title>Preturi extensii foaie de usa</v-list-item-title>
                </v-list-item-content>
            </v-list-item>
            <v-list-item
                link
                onclick="window.location.assign('/cabinet/operator/edit-extensii-product')">
                <v-list-item-action>
                    <v-icon>mdi-settings-outline</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                    <v-list-item-title>Extensii produs</v-list-item-title>
                </v-list-item-content>
            </v-list-item>
        </v-list-group>


        <v-list-item link>
            <v-list-item-action>
                <v-icon>mdi-logout</v-icon>
            </v-list-item-action>
            <v-list-item-content>
                <v-list-item-title
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Iesire din cont
                </v-list-item-title>
            </v-list-item-content>
        </v-list-item>
    </v-list>
</v-navigation-drawer>
<v-app-bar app color="indigo" dark class="d-flex">
    <div>
        {{--        <v-btn class="ma-2" fab color="teal" @click="drawer = !drawer">--}}
        {{--            <v-icon>mdi-format-list-bulleted-square</v-icon>--}}
        {{--        </v-btn>--}}
    </div>
</v-app-bar>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
