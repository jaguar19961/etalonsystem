<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
@include('site.pdf.style')
{{--{{dd($invoices)}}--}}
<header>
    <div class="flex">
        <table>
            <tr>
                <td width="33%">
                    <span>Furnizor: VIPROCELL SRL</span>
                    <span>Reg. com.: J08/2165/2017</span>
                    <span>CIF:RO37998555</span>
                    <span>Adresa: Str. Stadionului 1, Brasov, Jud. Brasov</span>
                    <span>IBAN: RO77RZBR0000060019559736</span>
                    <span>Banca: RAIFFEISEN BANK</span><br>
                    <span>Capital social: 200 Lei</span>
                </td>
                <td class="info-box" width="33%">
                    <h3>Factura</h3>
                    <div class="info">
                        <span> Seria PD nr. 1008 </span> <br>
                        <span>Data (zi/luna/an): 05/02/2020</span> <br>
                        <span>Cota TVA: 19%</span>
                    </div>
                    <div class="partner-logo">
                        <img src="{{asset('images/partners-img/profildoors.png')}}" alt="">
                    </div>
                </td>
                <td width="33%" style="text-align: right">
                    <span>Furnizor: VIPROCELL SRL</span>
                    <span>Reg. com.: J08/2165/2017</span>
                    <span>CIF:RO37998555</span>
                    <span>Adresa: Str. Stadionului 1, Brasov, Jud. Brasov</span>
                    <span>IBAN: RO77RZBR0000060019559736</span>
                    <span>Banca: RAIFFEISEN BANK</span><br>
                    <span>Capital social: 200 Lei</span>
                </td>
            </tr>
        </table>
    </div>
</header>
<footer></footer>
<main>
    <div>
        <table class="invoice-table">
            <tr class="header">
                <td style="width: 5%">Nr.crt</td>
                <td>Denumirea produselor sau a serviciilor</td>
                <td style="width: 5%">U.M.</td>
                <td style="width: 5%">Cant.</td>
                <td style="width: 10%">Pret unitar (fara TVA) -Lei-</td>
                <td style="width: 8%">Valoarea -Lei-</td>
                <td style="width: 8%">Valoarea TVA -Lei-</td>
            </tr>
            <tr class="count">
                <td style="width: 5%">0</td>
                <td>1</td>
                <td style="width: 5%">2</td>
                <td style="width: 5%">3</td>
                <td style="width: 10%">4</td>
                <td style="width: 8%">5</td>
                <td style="width: 8%">6</td>
            </tr>

            @php
            $counter = 1;
            @endphp
            @foreach($invoices as $ke => $in)
                @foreach($in->carts as $key=> $inv)

                <tr class="obj">
                    <td style="width: 5%">{{$counter++}}</td>
                    <td style="text-align: left">{{$inv->seria}} de
                        culoare {{$inv->color}} {{$inv->window}} {{$inv->inaltime}}*{{$inv->latime}} {{$inv->muchii}}
                        {{$inv->tip_balama}}</td>
                    <td style="width: 5%">pcs</td>
                    <td style="width: 5%">{{$inv->quantity}}</td>
                    <td style="width: 10%">{{$inv->foaie_de_usa_total_price - ($inv->foaie_de_usa_total_price / 100) * 19 * $inv->quantity}}</td>
                    <td style="width: 8%">{{$inv->foaie_de_usa_total_price}}</td>
                    <td style="width: 8%">{{($inv->foaie_de_usa_total_price / 100) * 19 * $inv->quantity}}</td>
                </tr>
                <tr class="obj">
                    <td style="width: 5%">{{$counter++}}</td>
                    <td style="text-align: left">Balamale {{$inv->tip_balama}} {{$inv->color}}</td>
                    <td style="width: 5%">pcs</td>
                    <td style="width: 5%">{{$in->balamale['sum_nr_de_balamale_Eclipse']}}</td>
                    <td style="width: 10%">{{$in->balamale['sum_balama_price_Eclipse'] - ($in->balamale['sum_balama_price_Eclipse'] / 100) * 19 }}</td>
                    <td style="width: 8%">{{$in->balamale['sum_balama_price_Eclipse']}}</td>
                    <td style="width: 8%">{{($in->balamale['sum_balama_price_Eclipse'] / 100) * 19 }}</td>
                </tr>

                    @if(!isset($in->broaste['sum_broasca_price_Wc']))
                        <tr class="obj">
                            <td style="width: 5%">{{$counter++}}</td>
                            <td style="text-align: left">Broaste Polaris WC</td>
                            <td style="width: 5%">pcs</td>
                            <td style="width: 5%">{{$in->broaste['sum_broasca_contra_placi_quantity_Wc']}}</td>
                            <td style="width: 10%">{{$in->broaste['sum_broasca_price_Wc'] - ($in->broaste['sum_broasca_price_Wc'] / 100) * 19 }}</td>
                            <td style="width: 8%">{{$in->broaste['sum_broasca_price_Wc']}}</td>
                            <td style="width: 8%">{{($in->broaste['sum_broasca_price_Wc'] / 100) * 19 }}</td>
                        </tr>
                    @endif
                    @if(!isset($in->broaste['sum_broasca_price_Cilindru']))
                        <tr class="obj">
                            <td style="width: 5%">{{$counter++}}</td>
                            <td style="text-align: left">Broaste Polaris Cilindru</td>
                            <td style="width: 5%">pcs</td>
                            <td style="width: 5%">{{$in->broaste['sum_broasca_contra_placi_quantity_Cilindru']}}</td>
                            <td style="width: 10%">{{$in->broaste['sum_broasca_price_Cilindru'] - ($in->broaste['sum_broasca_price_Cilindru'] / 100) * 19 }}</td>
                            <td style="width: 8%">{{$in->broaste['sum_broasca_price_Cilindru']}}</td>
                            <td style="width: 8%">{{($in->broaste['sum_broasca_price_Cilindru'] / 100) * 19 }}</td>
                        </tr>
                    @endif
                <tr class="obj">
                    <td style="width: 5%">{{$counter++}}</td>
                    <td style="text-align: left">Grinda {{$inv->color}}</td>
                    <td style="width: 5%">pcs</td>
                    <td style="width: 5%">{{$inv->grinda_quantity}}</td>
                    <td style="width: 10%">{{$inv->grinda_price - ($inv->grinda_price / 100) * 19 }}</td>
                    <td style="width: 8%">{{$inv->grinda_price}}</td>
                    <td style="width: 8%">{{($inv->grinda_price / 100) * 19 }}</td>
                </tr>
                <tr class="obj">
                    <td style="width: 5%">{{$counter++}}</td>
                    <td style="text-align: left">Traverse verticale {{$inv->color}}</td>
                    <td style="width: 5%">pcs</td>
                    <td style="width: 5%">{{$inv->traverse_verticale_qunatity}}</td>
                    <td style="width: 10%">{{$inv->traversa_verticala_price - ($inv->traversa_verticala_price / 100) * 19 }}</td>
                    <td style="width: 8%">{{$inv->traversa_verticala_price}}</td>
                    <td style="width: 8%">{{($inv->traversa_verticala_price / 100) * 19 }}</td>
                </tr>
                    @endforeach
            @endforeach

            <tr class="footer">
                <td colspan="7" style="text-align: left">Comanda 87 Dinu Cristian</td>
            </tr>
            <tr class="footer">
                <td rowspan="3" colspan="4" style="text-align: left; padding: 0px 0px 5px 5px !important;">
                    <span>Intocmit de: Gabriela Maria Fulop</span> <br>
                    <span>CNP: -</span> <br>
                    <span>Numele delegatului: -</span> <br>
                    <span>B.I/C.I: -</span> <br>
                    <span>Mijloc transport: -</span> <br>
                    <span>Expedierea s-a efectuat in prezenta noastra la data de ....................ora.........</span><br>
                    <span>Semnaturile:</span>
                </td>
                <td>Total</td>
                <td>{{$total}}</td>
                <td>{{($total / 100) * 19}}</td>
            </tr>
            <tr class="footer">
                <td>Total plata</td>
                <td colspan="2">{{$total}}</td>
            </tr>
            <tr class="footer">
                <td colspan="3" style="text-align: left">Semnatura de primire:</td>
            </tr>
        </table>
    </div>

</main>
</body>
</html>
