<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Generare excel comanda</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @include('site.pdf.style')
</head>
<body>

<div id="app" data-app>
    {{--    <proforma-client :invoice_id="{{json_encode($invoice_id)}}"></proforma-client>--}}
    <excel-export :user_data="{{json_encode(Auth::user()->userInfo)}}" :invoice_id="{{json_encode($invoice_id)}}"></excel-export>
    {{--    <pdf-operator :invoice_id="{{json_encode($invoice_id)}}"></pdf-operator>--}}
</div>


</body>
</html>
