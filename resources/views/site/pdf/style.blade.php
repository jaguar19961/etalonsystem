<style>
    .flex table {
        border: 1px solid;
    }

    .flex .partner-logo {
        text-align: center;
        margin-top: 10px;
    }

    .flex .partner-logo img{
        max-width: 100px;
    }

    .flex .info-box{
        text-align: center;
    }

    .flex .info-box h3{
        font-size: 16pt;
    }

    .flex .info{
        text-align: center;
        border: 1px solid;
    }

    header{
        font-size: 10pt;
    }


    main .invoice-table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        font-size: 8pt;
        margin-top: 10px;
    }

    main .invoice-table td, main .invoice-table th {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: center;
    }

    main .invoice-table tr:nth-child(even){background-color: #f2f2f2;}

    main .invoice-table tr:hover {background-color: #ddd;}

    main .invoice-table th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
    }
</style>
