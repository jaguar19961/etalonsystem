@extends('site.layouts.app')
@section('content')
    <elemente-toc :colors="{{json_encode($colors)}}"
                  :seria="{{json_encode($seria)}}"
                  :extension_settings="{{$extension_settings}}"></elemente-toc>
@endsection
