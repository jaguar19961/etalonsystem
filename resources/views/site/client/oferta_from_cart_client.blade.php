<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        @page {
            size: auto;
            margin: 1mm;
        }
    </style>
</head>
<body style="background: #4c4c4c;">
<div id="app">
    <oferta-cos :offer="{{json_encode($cart)}}" :invoice="{{json_encode($invoice)}}" :total="{{json_encode($total)}}" :user="{{json_encode(auth()->user())}}"></oferta-cos>
</div>
</body>
</html>
