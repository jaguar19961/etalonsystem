@extends('site.layouts.app')
@section('content')
    <oferta-usa :type="{{json_encode($type)}}" :offer="{{json_encode($offer)}}"></oferta-usa>
@endsection
