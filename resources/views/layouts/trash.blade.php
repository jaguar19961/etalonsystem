<h4>Pentru:{{cart.pentru_cine}} | Amplasare: {{cart.amplasare}} </h4>
<div class="specs"
     v-if="cart.specification.foaie_de_usa.length > 0 || cart.specification.foaie_de_usa != ''">
                   <span>Foaie de usa:
                       {{cart.specification.foaie_de_usa.seria}}
                       {{cart.specification.foaie_de_usa.culoare}}
                       {{cart.specification.foaie_de_usa.window}} |
                       {{cart.specification.foaie_de_usa.inaltime}}cm x {{cart.specification.foaie_de_usa.latime}}cm |
                       Grosime canat: {{cart.specification.foaie_de_usa.grosime_canat}}mm
                       <span class="text-red">{{cart.specification.foaie_de_usa.quantity}}set x {{cart.specification.foaie_de_usa.price}} = {{cart.specification.foaie_de_usa.total}}euro</span>
                   </span>
</div>
<div class="specs" v-if="cart.specification.traverse.length > 0 || cart.specification.traverse != ''">
                   <span>Traverse verticale:
                       {{cart.specification.traverse.culoare}}|
                       {{cart.specification.traverse.inaltime}}cm |
                       Latime toc: {{cart.specification.traverse.latime_toc}}mm |
                       Grosime canat: {{cart.specification.traverse.grosime_canat}}mm |
                       Pervaz: {{cart.specification.traverse.pervaz}}mm
                       <span class="text-red">{{cart.specification.traverse.quantity}}set x {{cart.specification.traverse.price}} = {{cart.specification.traverse.total}} euro</span>
                   </span>
</div>
<div class="specs" v-if="cart.specification.grinda.length > 0 || cart.specification.grinda != ''">
                   <span>Grinda:
                       {{cart.specification.grinda.culoare}} |
                       {{cart.specification.grinda.latime}}cm |
                       Latime toc: {{cart.specification.grinda.latime_toc}}mm |
                       Grosime canat: {{cart.specification.grinda.grosime_canat}}mm |
                       Pervaz: {{cart.specification.grinda.pervaz}}mm
                       <span class="text-red">{{cart.specification.grinda.quantity}}set x {{cart.specification.grinda.price}} = {{cart.specification.grinda.total}} euro</span>
                   </span>
</div>
<div class="specs" v-if="cart.specification.balamale.length > 0 || cart.specification.balamale != ''">
                   <span>Balama: {{cart.specification.balamale.tip_balama}}
                       <span class="text-red">{{cart.specification.balamale.quantity}}set x {{cart.specification.balamale.price}} = {{cart.specification.balamale.total}} euro</span>
                   </span>
</div>
<div class="specs" v-if="cart.specification.broasca.length > 0 || cart.specification.broasca != ''">
                   <span>Broasca: {{cart.specification.broasca.broasca}}
                       <span class="text-red">{{cart.specification.broasca.quantity}}set x {{cart.specification.broasca.price}} = {{cart.specification.broasca.total}} euro</span>
                   </span>
</div>
<div class="specs"
     v-if="cart.specification.extensii_verticale.length > 0 || cart.specification.extensii_verticale != ''">
                   <span>Extensii verticale:
                       {{cart.specification.extensii_verticale.latimea}}mm
                       <span class="text-red">{{cart.specification.extensii_verticale.quantity}}set x {{cart.specification.extensii_verticale.price}} = {{cart.specification.extensii_verticale.total}} euro</span>
                   </span>
</div>
<div class="specs"
     v-if="cart.specification.extensii_orizontale.length > 0 || cart.specification.extensii_orizontale != ''">
                   <span>Extensii orizontale:
                       {{cart.specification.extensii_orizontale.latimea}}mm
                       <span class="text-red">{{cart.specification.extensii_orizontale.quantity}}set x {{cart.specification.extensii_orizontale.price}} = {{cart.specification.extensii_orizontale.total}} euro</span>
                   </span>
</div>
<div class="total">
    <span></span>
    <span class="text-red">Total: {{cart.total}}euro</span>
</div>
