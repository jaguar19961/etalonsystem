<?php

use App\Http\Controllers\ApiController;
use \App\Http\Controllers\ColorsController;
use \App\Http\Controllers\WindowsController;
use Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('optimize:clear');
    return redirect()->back();
});
Route::get('/clear-config', function() {
    $exitCode = Artisan::call('clear-compiled ');
    return redirect()->back();
});

Route::get('/feed', 'FeedController@export');
Route::get('/feed/download', 'FeedController@download');

Route::get('/', function () {
    return view('site.auth');
});

Route::get('/pdf', function () {
    return view('site.pdf.pdf');
});
Route::get('/get-pdf/{group_id}/{order_id}', 'PdfController@generate');

Route::post('utlizator-logare', 'RegisterController@logare');

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'cabinet', 'as' => 'cabinet.'], function () {
    Route::group(['prefix' => 'client', 'as' => 'client.'], function () {
        Route::get('contul-meu', 'ClientController@contulMeu');
        Route::get('comanda-noua', 'ClientController@newOrder');
        Route::get('feronerie', 'ClientController@feronerie');
        Route::get('elemente-toc', 'ClientController@elementeToc');
        Route::get('plinta', 'ClientController@plinta');
        Route::get('cos', 'ClientController@cart');
        Route::get('facturi', 'ClientController@myInvoices');
        Route::get('statistica', 'ClientController@statistics');
        Route::get('configurari', 'ClientController@config');
//        ### PDF ###
        Route::get('/pdf/{invoice_id}', 'PdfController@clientPdf');
        Route::get('/oferte-clienti', 'OfferClientController@index');
        Route::get('/get-offer/{id}', 'OfferClientController@getOffer');
        Route::get('/get-offer/{product_id}/{seria_id}/{color_id}/{window_id}/{price}/{cart_id}', 'OfferClientController@getOfferDoor');
        Route::get('/offer-for-client/{invoice_id}', 'OfferClientController@getOfferDoorForClientSaved');

    });


    Route::group(['prefix' => 'operator', 'as' => 'operator.'], function () {
        Route::get('contul-meu', 'OperatorController@contulMeu');
        Route::get('parteneri', 'OperatorController@parteneri');
        Route::get('facturi', 'OperatorController@invoice');
        Route::get('facturi-fabrica', 'OperatorController@invoiceManufacture');
        Route::get('editare-produse', 'OperatorController@editProduct');
        Route::get('configurari', 'OperatorController@config');

//        ### PRODUSE ###
        Route::get('plinta', 'OperatorController@plinta');
        Route::get('minere', 'OperatorController@minere');
//        ### PRODUSE ###

        //        ### PDF ###
        Route::get('/pdf/{invoice_id}', 'PdfController@operatorPdf');
        Route::get('/get-group/{group_id}', 'ManufactureController@GetGroupManufacturePdf');


        Route::get('/excel/{invoice_id}', 'PdfController@operatorExcel');

        //culori
        Route::get('/editare-culori', [ColorsController::class, 'index']);

        //molding
        Route::get('/editare-molding', [\App\Http\Controllers\MoldingController::class, 'index']);

        //insertii
        Route::get('/editare-insertii', [WindowsController::class, 'index']);

        Route::get('/edit-extensii-product', [\App\Http\Controllers\ExtensiiProductController::class, 'index']);

        Route::get('/tags', [\App\Http\Controllers\TagController::class, 'index']);
        Route::get('/series', [\App\Http\Controllers\SeriesController::class, 'index']);

    });
});

Route::group(['prefix' => 'axios', 'as' => 'axios.'], function () {
    Route::get('/get-settings-list', 'ConfigController@getSettingsList');
    Route::post('/save-settings-list', 'ConfigController@saveSettingsList');


    Route::post('/set-order-door', 'OperatorController@SetOrderDoor');

    Route::post('/save-price-for-doors', 'OperatorController@savePriceForDoors');
    Route::get('/get-doors/{id}', 'OperatorController@getDoors');
    Route::get('/get-partners', 'OperatorController@getPartners');
    Route::post('/save-partner-data', 'OperatorController@savePartnersData');
    Route::get('/get-series', 'OperatorController@getSeries');
    Route::get('/client/get-series', 'DoorsController@getSeries');
    Route::get('/client/get-serie/{id}', 'DoorsController@getSerie');
    Route::post('/client/save-offer', 'OfferClientController@saveOffer');
    Route::get('/client/get-offers', 'OfferClientController@getOffers');
    Route::get('/client/del-offers/{id}', 'OfferClientController@delOffers');


//    systems get
    Route::get('/get-colors-window', 'OperatorController@getWindowColor');
    Route::post('/save-new-door', 'DoorsController@saveDoor');


//    stergere usa daca este sticla sau culoare
    Route::get('/delete-door/{type}/{id}', 'OperatorController@deleteDoor');

//    client
    Route::get('/get-invoices', 'ClientController@getInvoices');

//    operator
    Route::get('/get-operator-invoices/{type}', 'OperatorController@getInvoices');
    Route::get('/operator-invoices-state/{process}/{invoice_id}', 'OperatorController@InvoiceState');

    Route::get('/operator-invoices-sendToManufacture/{group_id}', 'OperatorController@SendToManufacture');
//    cart
    Route::post('/add-to-cart', 'CartController@addToCart');
    Route::post('/add-to-cart-partial', 'CartController@addToCartPartials');

    Route::get('/get-cart', 'CartController@getCart');
    Route::get('/get-cart-pdf/{invoice_id}', 'CartController@getCartPdf');
    Route::get('/get-cart-pdf-operator/{invoice_id}', 'CartController@getCartPdfOperator');


    Route::get('/delete-item-cart/{id}', 'CartController@deleteItemCart');
    Route::post('/finish-order', 'CartController@finishOrder');


//    extensiile congif
    Route::post('/save-extensii-orizontale', 'OperatorController@saveExtensiiOrizontale');


//    ### CONFIGURARA GRUP FACTURI ###
    Route::get('/get-groups', 'ManufactureController@GetGroups');
    Route::get('/get-group/{group_id}', 'ManufactureController@GetGroupManufactureData');
    Route::get('/end-group-orders/{id}/{status}', 'ManufactureController@EndGroupOrder');


//    ### tragere produse ###
//        ### MINERE ###
    Route::get('/get-minere', 'MinereController@GetMinere');
    Route::post('/save-minere', 'MinereController@SaveMinere');
    Route::get('/delete-minere/{id}', 'MinereController@DelMinere');
    //        ### END MINERE ###

//    ### PLINTA ###
    Route::get('/get-plinta', 'PlintaController@GetPlinta');
    Route::post('/save-plinta', 'PlintaController@SavePlinta');
    Route::get('/delete-plinta/{id}', 'PlintaController@DelPlinta');
//    ### END PLINTA ###

//    ### EXTENSII ###
    Route::post('/getExtensiiOrizontale', 'ElementeController@GetExtensiiOri');
    Route::post('/getExtensiiVerticale', 'ElementeController@GetExtensiiVer');

//    ### END EXTENSII ###
    Route::post('/get-door-image', 'OfferClientController@getDoorImage');


    Route::get('/delete-operator-invoices/{invoice_id}', 'CartController@deleteOperatorInvoice');

//    culori
    Route::get('/get-colors', [ColorsController::class, 'getColors']);
    Route::post('/save-color', [ColorsController::class, 'store']);
    Route::get('/delete-color/{id}', [ColorsController::class, 'destroy']);

    //    molding
    Route::get('/get-moldings', [\App\Http\Controllers\MoldingController::class, 'getMoldings']);
    Route::post('/save-molding', [\App\Http\Controllers\MoldingController::class, 'store']);
    Route::post('/store-door-molding', [\App\Http\Controllers\MoldingController::class, 'storeDoor']);
    Route::post('/delete-door-molding/{id}', [\App\Http\Controllers\MoldingController::class, 'deleteDoor']);
    Route::get('/delete-molding/{id}', [\App\Http\Controllers\MoldingController::class, 'destroy']);

    //edit extensii product
    Route::get('/get-extensii-product', [\App\Http\Controllers\ExtensiiProductController::class, 'getExtensiiProduct']);
    Route::post('/update-extensii-product', [\App\Http\Controllers\ExtensiiProductController::class, 'update']);


    //    windows
    Route::get('/get-windows', [WindowsController::class, 'getWindows']);
    Route::post('/save-window', [WindowsController::class, 'store']);
    Route::get('/delete-window/{id}', [WindowsController::class, 'destroy']);

    //tags-----------------------
    Route::post('/tags/store', [\App\Http\Controllers\TagController::class, 'store']);
    Route::put('/tags/update/{id}', [\App\Http\Controllers\TagController::class, 'update']);
    Route::delete('/tags/delete/{id}', [\App\Http\Controllers\TagController::class, 'update']);
        //operator-----------
    Route::get('/tags/get', [\App\Http\Controllers\TagController::class, 'get']);
    Route::post('/tags/save-tag', [\App\Http\Controllers\TagController::class, 'saveTag']);

    //serie-----------------------
    Route::get('/series/get', [\App\Http\Controllers\SeriesController::class, 'get']);
    Route::post('/series/store', [\App\Http\Controllers\SeriesController::class, 'store']);
    Route::put('/series/update/{id}', [\App\Http\Controllers\SeriesController::class, 'update']);
    Route::get('/series/delete/{id}', [\App\Http\Controllers\SeriesController::class, 'destroy']);
});

Auth::routes();

