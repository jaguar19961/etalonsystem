<?php

use Illuminate\Http\Request;
use \App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/product-series/{serie_id?}', [ApiController::class, 'get']);
Route::get('/series', [ApiController::class, 'getSeries']);
Route::get('/serie/{id}', [ApiController::class, 'getSerie']);
Route::get('/product/{id}', [ApiController::class, 'getProduct']);
Route::get('/product-similar/{series_id}', [ApiController::class, 'getSimilar']);
Route::get('/feronerie', [ApiController::class, 'getFeronerie']);
Route::get('/colors/{series_id?}', [ApiController::class, 'getColors']);
Route::get('/windows/{series_id?}', [ApiController::class, 'getWindows']);
Route::get('/tags', [ApiController::class, 'getTags']);
//new API
Route::get('/catalog', [ApiController::class, 'catalog']);

