<?php

namespace App\Http\Controllers;

use App\Products;
use App\ProductTag;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TagController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('site.operator.tags');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        if ($request->id != null) {
            $minere = Tag::findOrFail($request->id);
        } else {
            $minere = new Tag();
        }
        $minere->name = $request->name;
        $minere->code = $request->code;
        if ($request->get('image')) {
            $image = $request->get('image');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('/images/colors/') . $name);
            $minere->image = '/images/colors/' . $name;
        }
        $minere->save();
        return response(['status' => 'success']);
        return response('message', 201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response('message', 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Tag::findOrFail($id);
        File::delete(public_path($model->image));
        $model->delete();
        return response(['status' => 'success']);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function get()
    {
        $data = Tag::orderBy('created_at', 'desc')->get();
        return response(['status' => 'success', 'data' => $data], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function saveTag(Request $request)
    {
        ProductTag::where('product_id', $request->get('product_id'))->delete();
        foreach ($request->get('tags') as $item) {
            ProductTag::create([
                'product_id' => $request->get('product_id'),
                'tag_id' => $item,
            ]);
        }
        return response('message', 200);
    }


}
