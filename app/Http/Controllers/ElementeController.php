<?php

namespace App\Http\Controllers;

use App\Colors;
use App\ExtensiiOrizontalePivot;
use App\ExtensiiOrizontalePrice;
use App\ExtensiiVerticalePivot;
use App\ExtensiiVerticalePrice;
use App\Products;
use Illuminate\Http\Request;

class ElementeController extends Controller
{
    public function GetExtensiiOri(Request $request){
//        dd($request);
        $val = 0;
        if ($request->extensii_oriz_select == 'single'){
            $val = 0;
        }
        if ($request->extensii_oriz_select == 'double'){
            $val = 1;
        }

        $extensii_type = ExtensiiOrizontalePivot::where('seria_id', $request->extensii_oriz_select_seria)->first();
        $color = array();
        foreach ($extensii_type->products as $colors){
            foreach ($colors->colors as $productcolor){
                $color[] = $productcolor->color->name;
            }
        }
        $collection = collect($color);
        $color = $collection->unique()->values()->all();

        $extensii = ExtensiiOrizontalePrice::where('grup', $extensii_type->extension_id)->where('tip', $val)->get();
        return response(['status' => 'success', 'extensii' => $extensii, 'color' => $color]);
    }

    public function GetExtensiiVer(Request $request){
//        dd($request->all());
        $val = 0;
        if ($request->extensii_vert_select == 'single'){
            $val = 0;
        }
        if ($request->extensii_vert_select == 'double'){
            $val = 1;
        }

        $extensii_type = ExtensiiVerticalePivot::where('seria_id', $request->extensii_vert_select_seria)->with('products')->first();

        $color = array();
        foreach ($extensii_type->products as $colors){
            foreach ($colors->colors as $productcolor){
                $color[] = $productcolor->color->name;
            }
        }
        $collection = collect($color);
        $color = $collection->unique()->values()->all();

        $extensii = ExtensiiVerticalePrice::where('grup', $extensii_type->extension_id)->get();
//        $colors = Colors::get();

        return response(['status' => 'success', 'extensii' => $extensii, 'color' => $color]);
    }
}
