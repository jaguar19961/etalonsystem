<?php

namespace App\Http\Controllers;

use App\Serie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SeriesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('site.operator.series');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        if ($request->id != null) {
            $minere = Serie::findOrFail($request->id);
        } else {
            $minere = new Serie();
        }
        $minere->name = $request->name;
        $minere->description = $request->description;
        if ($request->get('image')) {
            $image = $request->get('image');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('/images/colors/') . $name);
            $minere->images = '/images/colors/' . $name;
        }
        $minere->save();
        return response('message', 201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response('message', 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Serie::findOrFail($id);
        $model->delete();
//        if (count($model->products) === 0) {
//            File::delete(public_path($model->image));
//            $model->delete();
//            return response(['status' => 'success'], 200);
//        }else{
//            return response(['status' => 'Some product is attached to this model'], 422);
//        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function get()
    {
        $data = Serie::orderBy('created_at', 'desc')->get();
        return response(['status' => 'success', 'data' => $data], 200);
    }
}
