<?php

namespace App\Http\Controllers;

use App\Cart;
use App\ExtensiiOrizontalePrice;
use App\GrindaPivot;
use App\GrindaPrice;
use App\Invoice;
use App\TraversaVerticalaPivot;
use App\TraversaVerticalaPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\Rule;
use function GuzzleHttp\Promise\all;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {


        $this->validate($request, [
            'amplasare' => 'required',

            'extensii_verticale.quantity' => Rule::requiredIf($request->all['extensii_verticale_off'] == true),
            'extensii_verticale.latime' => Rule::requiredIf($request->all['extensii_verticale_off'] == true),
            'extensii_orizontale.quantity' => Rule::requiredIf($request->all['extensii_orizontale_off'] == true),
            'extensii_orizontale.latime' => Rule::requiredIf($request->all['extensii_orizontale_off'] == true),

            'grinda.quantity' => Rule::requiredIf($request->grinda_off == true),

            'traverse_verticale.quantity' => Rule::requiredIf($request->traverse_off == true),

            'foaie_de_usa.quantity' => Rule::requiredIf($request->foaie_de_usa_off == true),
            'balamale.quantity' => Rule::requiredIf($request->balamale_off == true),
            'broasca.quantity' => Rule::requiredIf($request->broasca_off == true),


        ]);
//        $decoded = json_decode($request->all());
        $filtered = $request->except('invoice_id', 'total', 'pentru_cine', 'amplasare', 'all');


        if ($request->invoice_id != null) {
            $cart = new Cart();
            $cart->user_id = Auth::user()->id;
            $cart->invoice_id = $request->invoice_id;
            $cart->specification = $filtered;
            $cart->pentru_cine = $request->pentru_cine;
            $cart->amplasare = $request->amplasare;
            $cart->total = $request->total;
            $cart->save();

            return response(['status' => 'success']);
        } else {
            $invoice = new Invoice();
            $invoice->user_id = Auth::user()->id;
            $invoice->status = 1;
            $invoice->save();

            $cart = new Cart();
            $cart->user_id = Auth::user()->id;
            $cart->invoice_id = $invoice->id;
            $cart->specification = $filtered;
            $cart->pentru_cine = $request->pentru_cine;
            $cart->amplasare = $request->amplasare;
            $cart->total = $request->total;
            $cart->save();
            return response(['status' => 'success']);
        }


    }

    public function addToCartPartials(Request $request)
    {
        $invoic = Invoice::where('user_id', auth()->id())->where('status', 1)->first();
        $total = 0;
        $type = null;

        if ($request['minere']['total'] != 0) {
            $total = $request['minere']['total'];
            $type = 6;
        }


        if ($request->balamale['total'] != 0) {
            $total = $request->balamale['total'];
            $type = 2;
        }

        if ($request->broasca['total'] != 0) {
            $total = $request->broasca['total'];
            $type = 3;
        }

        if ($request->grinda['total'] != 0) {
            $total = $request->grinda['total'];
            $type = 4;
        }

        if ($request->traverse_verticale['total'] != 0) {
            $total = $request->traverse_verticale['total'];
            $type = 5;
        }

        if ($request->extensii_orizontale['total'] != 0) {
            $total = $request->extensii_orizontale['total'];
            $type = 8;
        }

        if ($request->extensii_verticale['total'] != 0) {
            $total = $request->extensii_verticale['total'];
            $type = 9;
        }

        if ($request->plinta['total'] != 0) {
            $total = $request->plinta['total'];
            $type = 7;
        }


        if ($invoic != null) {

            $cart = new Cart();
            $cart->user_id = auth()->id();
            $cart->invoice_id = $invoic->id;
            $cart->specification = $request->all();
            $cart->pentru_cine = null;
            $cart->product_type_id = $type;
            $cart->amplasare = null;
            $cart->total = $total;
            $cart->save();

            return response(['status' => 'success']);
        } else {
            $invoice = new Invoice();
            $invoice->user_id = auth()->id();
            $invoice->status = 1;
            $invoice->save();

            $cart = new Cart();
            $cart->user_id = auth()->id();
            $cart->invoice_id = $invoice->id;
            $cart->specification = $request->all();
            $cart->pentru_cine = null;
            $cart->product_type_id = $type;
            $cart->amplasare = null;
            $cart->total = $total;
            $cart->save();
            return response(['status' => 'success']);
        }
    }

    public function getCart()
    {
        $carts = Invoice::where('user_id', auth()->id())->where('status', 1)->with('carts')->first();
        if ($carts == null) {
            return response(['status' => 'nocart']);
        } else {
            $cart_id = $carts->id;
            $cart = $carts->carts;
            $sum_by_name = $carts->name_sum;
            $cart_for = $carts->pentru_cine;
            $cartsum = Cart::where('invoice_id', $carts->id)->sum('total');

//            filter product by category and redurn total with
            $total = 0;
            foreach ($cart as $items) {
                if ($items->product_type_id == 6) {
                    $total += $items->total - (($items->total * Auth::user()->userInfo->discount_minere) / 100);
                } else if ($items->product_type_id == 1 || $items->product_type_id == 7) {
                    $total += $items->total - (($items->total * Auth::user()->userInfo->discount) / 100);
                } else {
                    $total += $items->total;
                }
            }

            return response(['status' => 'success', 'cart' => $cart, 'total' => $cartsum, 'cart_id' => $cart_id, 'sum_by_name' => $sum_by_name, 'cart_for' => $cart_for, 'total_without_TVA' => $total]);
        }

    }

    public function getCartPdf($invoice_id)
    {
//        dd($invoice_id);
        $carts = Invoice::where('user_id', auth()->id())->where('id', $invoice_id)->with('carts')->first();
        $send_date = $carts->created_at;
//        dd($carts);
        if ($carts == null) {
            return response(['status' => 'nocart']);
        } else {
            $cart_id = $carts->id;
            $cart = $carts->carts;
            $sum_by_name = $carts->name_sum;
            $cart_for = $carts->pentru_cine;
            $cartsum = Cart::where('invoice_id', $carts->id)->sum('total');
            $with_tva_dealer = $carts->total_with_discount_tva;
            $with_wighout_tva_dealer = $carts->total_with_discount_tva;
//            dd($carts);

            return response(['status' => 'success', 'cart' => $cart, 'total' => $cartsum, 'cart_id' => $cart_id, 'sum_by_name' => $sum_by_name, 'send_date' => $send_date, 'cart_for' => $cart_for, 'withtvadealer' => $with_tva_dealer, 'with_wighout_tva_dealer' => $with_wighout_tva_dealer]);
        }

    }

    public function getCartPdfOperator($invoice_id)
    {
//        dd($invoice_id);
        $carts = Invoice::where('id', $invoice_id)->with('carts')->first();
        $send_date = $carts->created_at;
        $de_la = $carts->userInfo->name;
        $user = $carts->userInfo;
        $pentru_cine = $carts->pentru_cine;
        $discount = $carts->userDetails->discount;
        $discount_minere = $carts->userDetails->discount_minere;
        $userbank = $carts->userDetails->bank_details;
        $created = $carts->created_at;
        $cartSumWithDiscount = $carts->total_with_discount_tva;
//        dd($carts);
        if ($carts == null) {
            return response(['status' => 'nocart']);
        } else {
            $cart_id = $carts->id;
            $cart = $carts->carts;
            $sum_by_name = $carts->name_sum;
            $cartsum = Cart::where('invoice_id', $carts->id)->sum('total');
            return response(['status' => 'success',
                'cart' => $cart,
                'total' => $cartsum,
                'cart_id' => $cart_id,
                'sum_by_name' => $sum_by_name,
                'send_date' => $send_date,
                'de_la' => $de_la,
                'discount' => (int)$discount,
                'discount_minere' => (int)$discount_minere,
                'userbank' => $userbank,
                'created' => $created,
                'cartSumWithDiscount' => $cartSumWithDiscount,
                'user_info' => $user,
                'pentru_cine' => $pentru_cine]);
        }

    }

    public function deleteItemCart($id)
    {
        $itemCart = Cart::findOrFail($id);
        $countItemInCart = Cart::where('invoice_id', $itemCart->invoice_id)->count();

        if ($countItemInCart === 1) {
            $invoice = Invoice::findOrFail($itemCart->invoice_id);
            $invoice->delete();
            $itemCart->delete();
        } else {
            $itemCart->delete();
        }
        return response(['status' => 'success']);

    }

    public function finishOrder(Request $request)
    {

        $this->validate($request, [
            'pentru_cine' => 'required',
        ]);
//        dd($request->all());
        $getInvoice = Invoice::findOrFail($request->invoice_id);
        $getInvoice->status = 2;
        $getInvoice->process = 2;
        $getInvoice->pentru_cine = $request->pentru_cine;
        $getInvoice->total = $request->total;
        $getInvoice->total_with_discount = $request->total_with_discount;
        $getInvoice->total_with_discount_tva = $request->total_with_discount_tva;
        $getInvoice->save();
        return response(['status' => 'success']);
    }

    public function deleteOperatorInvoice($id)
    {
        $delCart = Cart::where('invoice_id', $id)->get();
        foreach ($delCart as $item) {
            $item->delete();
        }

        $delInvoice = Invoice::findOrFail($id);
        $delInvoice->delete();

        return response(['status' => 'success']);
    }
}
