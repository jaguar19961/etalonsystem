<?php

namespace App\Http\Controllers;


use App\Cart;
use App\ExtensiiOrizontalePrice;
use App\ExtensiiVerticalePrice;
use App\GrindaPivot;
use App\Invoice;
use App\Products;
use App\Serie;
use App\Colors;
use App\ListDetail;
use App\TraversaVerticalaPivot;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function contulMeu()
    {

        return view('site.client.dashboard');
    }

    public function newOrder()
    {
        $extension_settings = ListDetail::get();
        $doors = Serie::with('products')->get();
        return view('site.client.new-order', compact('doors', 'extension_settings'));
    }

    public function feronerie()
    {
        $colors = Colors::get();
        return view('site.client.feronerie', compact('colors'));
    }

    public function elementeToc()
    {
        $colors = Colors::get();
        $seria = Serie::get();
        $extension_settings = ListDetail::get();
        return view('site.client.elemente-toc', compact('colors', 'seria', 'extension_settings'));
    }

    public function plinta()
    {
        return view('site.client.plinta');
    }

    public function statistics()
    {
        return view('site.client.statistics');
    }

    public function config()
    {
        return view('site.client.config');
    }

    public function cart()
    {
        return view('site.client.cart');
    }

    public function myInvoices()
    {
        $invoices = Invoice::where('user_id', auth()->id())->where('status', 2)->with('carts')->get();

//        dd($invoices[0]->sum_carts);
        return view('site.client.invoices');
    }

    public function getInvoices()
    {
        $invoices = Invoice::where('user_id', auth()->id())->where('status', 2)->with('carts')->orderBy('created_at', 'desc')->get();

        if ($invoices == null) {
            return response(['status' => 'cosgol']);
        } else {
            return response(['status' => 'success', 'invoices' => $invoices]);
        }
    }

    public function getInvoice($invoice_id)
    {
        $invoices = Invoice::where('user_id', auth()->id())->where('id', $invoice_id)->where('status', 2)->with('carts')->orderBy('created_at', 'desc')->get();

        if ($invoices == null) {
            return response(['status' => 'cosgol']);
        } else {
            return response(['status' => 'success', 'invoices' => $invoices]);
        }

    }
}
