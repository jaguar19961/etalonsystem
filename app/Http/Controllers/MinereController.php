<?php

namespace App\Http\Controllers;

use App\Minere;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Image;

class MinereController extends Controller
{
    public function GetMinere()
    {

        $minere = Minere::get();
        return response(['status' => 'success', 'minere' => $minere]);
    }

    public function SaveMinere(Request $request)
    {
        if ($request->id != null){
            $minere = Minere::findOrFail($request->id);
        }else{
            $minere = new Minere();
        }
        $minere->name = $request->name;
        $minere->price = $request->price;
        if ($request->get('image')) {
            $image = $request->get('image');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('/images/minere/') . $name);
            $minere->image = '/images/minere/'.$name;
        }
        $minere->save();
        return response(['status' => 'success']);
    }

    public function DelMinere($id){
        $miner = Minere::findOrFail($id);
        $miner->delete();
        return response(['status' => 'success']);
    }

}
