<?php

namespace App\Http\Controllers;

use App\Molding;
use App\ProductMolding;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MoldingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site.operator.molding');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);

        if ($request->id != null) {
            $minere = Molding::findOrFail($request->id);
        } else {
            $minere = new Molding();
        }
        $minere->name = $request->name;
        $minere->code = $request->code;
        if ($request->get('image')) {
            $image = $request->get('image');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('/images/molding/') . $name);
            $minere->image = '/images/molding/' . $name;
        }
        $minere->save();
        return response(['status' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Molding::findOrFail($id);
        File::delete(public_path($model->image));
        $model->delete();
        return response(['status' => 'success']);
    }

    public function deleteDoor($id)
    {
        $model = ProductMolding::findOrFail($id);
        File::delete(public_path($model->image));
        $model->delete();
        return response(['status' => 'success']);
    }

    public function getMoldings()
    {
        $data = Molding::orderBy('created_at', 'desc')->get();
        return response(['status' => 'success', 'data' => $data]);
    }

    public function storeDoor(Request $request)
    {
        if ($request->get('model_id') !== null) {
            $door = ProductMolding::find($request->get('model_id'));
        } else {
            $door = new ProductMolding();
        }

        $door->product_id = $request->product_id;
        $door->molding_id = $request->molding_id;
        $door->model_name = $request->model_name;
        $door->price = $request->price;

        if ($request->get('image')) {
            $path = public_path('/images' . '/catalog' . '/' . $door->image);
            File::delete($path);

            $path = public_path('/images' . '/catalog-img-original' . '/' . $door->image_original);
            File::delete($path);

            $image = $request->get('image');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->resize(null, 1500, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/catalog/') . $name);
            $door->image = $name;

            $name_original = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('images/catalog-img-original/') . $name_original);
            $door->image_original = $name_original;
        }

        $door->save();
        return response(['status' => 'success']);
    }
}
