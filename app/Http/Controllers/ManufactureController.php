<?php

namespace App\Http\Controllers;

use App\InvoiceGroup;
use Illuminate\Http\Request;

class ManufactureController extends Controller
{
    public function GetGroups(){
        $groups = InvoiceGroup::orderBy('id', 'desc')->where('status', 0)->where('to_manufacture', 1)->with('invoicesOrder')->get();

        return response(['status' => 'success', 'groupOrder' => $groups]);
    }

    public function GetGroupManufacturePdf($group_id){

        return view('site.operator.group-manufacture-pdf', compact('group_id'));
    }

    public function GetGroupManufactureData($group_id){
        $groups = InvoiceGroup::where('status', 0)->where('to_manufacture', 1)->where('id', $group_id)->with('invoicesOrder')->first();

        return response(['status' => 'success', 'group' => $groups]);
    }

    public function EndGroupOrder($id, $status) {
        $group = InvoiceGroup::findOrFail($id);
        $group->delivered = $status;
        $group->save();
        return response(['status' => 'success']);
    }
}
