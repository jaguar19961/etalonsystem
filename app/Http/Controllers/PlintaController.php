<?php

namespace App\Http\Controllers;

use App\Plinte;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\File;

class PlintaController extends Controller
{
    public function GetPlinta()
    {

        $minere = Plinte::get();
        return response(['status' => 'success', 'minere' => $minere]);
    }

    public function SavePlinta(Request $request)
    {
        if ($request->id != null){
            $minere = Plinte::findOrFail($request->id);
        }else{
            $minere = new Plinte();
        }
        $minere->name = $request->name;
        $minere->size = $request->size;
        $minere->price = $request->price;
        if ($request->get('image')) {
            $image = $request->get('image');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('/images/plinta/') . $name);
            $minere->image = '/images/plinta/'.$name;
        }
        $minere->save();
        return response(['status' => 'success']);
    }

    public function DelPlinta($id){
        $miner = Plinte::findOrFail($id);
        $miner->delete();
        return response(['status' => 'success']);
    }
}
