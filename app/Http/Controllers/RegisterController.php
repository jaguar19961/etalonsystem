<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function logare(LoginRequest $request)
    {
        $validated = $request->validated();

        $user = User::where('email', $validated['email'])->first();

        if ($user != null) {
            if (Auth::attempt(['email' => $user->email, 'password' => $validated['password']])) {
                $url = null;
                if ($user->user_type == 1) {
                    $url = '/cabinet/client/contul-meu';
                }
                if ($user->user_type == 2) {
                    $url = '/cabinet/operator/contul-meu';
                }
                if ($user->user_type == 3) {
                    $url = '/';
                }
                return response(['status' => 'success', 'user' => $user, 'url' => $url, 'nu' => 'Nu exista acest utilizator']);
            } else {
                return response(['status' => 'error', 'nu' => 'Nu exista acest utilizator']);
            }
        } else {
            return response(['status' => 'error', 'nu' => 'Nu exista acest utilizator']);
        }
    }
}
