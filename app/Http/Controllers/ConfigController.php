<?php

namespace App\Http\Controllers;

use App\ListDetail;
use App\ListType;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function getSettingsList(){
        $list = ListType::with('listDetails')->get();
        return response(['status' => 'success', 'list' => $list]);
    }

    public function saveSettingsList(Request $request) {
        foreach ($request->list_details as $list){
            $rewriteList = ListDetail::findOrFail($list['id']);
            $rewriteList->value = $list['value'];
            $rewriteList->save();
        }
        return response(['status' => 'success']);
    }
}
