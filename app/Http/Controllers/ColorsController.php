<?php

namespace App\Http\Controllers;

use App\Colors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ColorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site.operator.colors');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);

        if ($request->id != null) {
            $minere = Colors::findOrFail($request->id);
        } else {
            $minere = new Colors();
        }
        $minere->name = $request->name;
        $minere->code = $request->code;
        if ($request->get('image')) {
            $image = $request->get('image');
            $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('/images/colors/') . $name);
            $minere->image = '/images/colors/' . $name;
        }
        $minere->save();
        return response(['status' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Colors::findOrFail($id);
        File::delete(public_path($model->image));
        $model->delete();
        return response(['status' => 'success']);
    }

    public function getColors()
    {
        $data = Colors::orderBy('created_at', 'desc')->get();
        return response(['status' => 'success', 'data' => $data]);
    }
}
