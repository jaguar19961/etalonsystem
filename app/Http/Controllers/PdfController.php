<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\InvoiceGroup;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function generate($group_id, $order_id){

//        dd($group_id);

        if ($group_id != 'null'){
            $group = InvoiceGroup::where('id', $group_id)->first();
            $invoices = $group->invoicesOrder->load('carts');
        }

        if ($order_id != 'null'){
            $invoices = Invoice::where('id', $order_id)->with('carts')->get();
        }
//        dd($invoices);
//        calculare suma totala pentru invoice
        $total = 0;

        foreach($invoices as $key => $cart){
//            dd($cart->balamale);
            $total += $cart->balamale['sum_balama_price_Eclipse'];
            $total += $cart->broaste['sum_broasca_price_Cilindru'];
            $total += $cart->broaste['sum_broasca_price_Wc'];
            $total += $cart->traverse_verticale['sum_traversa_verticala_price'];
            $total += $cart->usi['sum_foaie_de_usa_total_price'];
            $total += $cart->usi['sum_grinda_price'];
        }

//        dd($total);

        $pdf = PDF::loadView('site/pdf/pdf', compact('invoices', 'total'))->setPaper('a4', 'portrait')->setWarnings(false)->save('myfile.pdf');
        return $pdf->stream();

    }

    public function clientPdf($invoice_id){

        return view('site/pdf/client/pdf', compact('invoice_id')) ;
    }

    public function operatorPdf($invoice_id){

        return view('site/pdf/operator/pdf', compact('invoice_id')) ;
    }

    public function operatorExcel($invoice_id){

        return view('site/pdf/operator/excel', compact('invoice_id')) ;
    }
}
