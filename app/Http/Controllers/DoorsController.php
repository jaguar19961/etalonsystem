<?php

namespace App\Http\Controllers;

use App\ProductColor;
use App\Products;
use App\Serie;
use App\ProductWindow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Image;

class DoorsController extends Controller
{
    public function saveDoor(Request $request)
    {
//        dd($request->all());
        if ($request->type == 0) {
            $door = new Products();
            $door->category_id = 1;
            $door->serie_id = $request->seria_id;
            $door->name = $request->name;
            $door->save();
        }

        if ($request->type == 1) {
            $door = new ProductColor();
            $door->product_id = $request->product_id;
            $door->color_id = $request->color_id;
            $door->price = $request->price;
            $door->price_matte = $request->price_matte;
            $door->price_crom = $request->price_crom;

            if ($request->get('image')) {
                $path = public_path('/images' . '/catalog' . '/' . $door->image);
                File::delete($path);

                $path = public_path('/images' . '/catalog-img-original' . '/' . $door->image_original);
                File::delete($path);

                $image = $request->get('image');
                $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->resize(null, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('images/catalog/') . $name);
                $door->image = $name;

                $name_original = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/catalog-img-original/') . $name_original);
                $door->image_original = $name_original;
            }

            $door->save();
        }

        if ($request->type == 2) {
//            dd($request->all());
            $door = new ProductWindow();
            $door->product_id = $request->product_id;
            $door->color_id = $request->product_color_id;
            $door->window_id = $request->window_id;
            $door->price = $request->price;
            $door->price_matte = $request->price_matte;
            $door->price_crom = $request->price_crom;

            if ($request->get('image')) {
                $path = public_path('/images' . '/catalog' . '/' . $door->image);
                File::delete($path);

                $path = public_path('/images' . '/catalog-img-original' . '/' . $door->image_original);
                File::delete($path);

                $image = $request->get('image');
                $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->resize(null, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('images/catalog/') . $name);
                $door->image = $name;

                $name_original = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/catalog-img-original/') . $name_original);
                $door->image_original = $name_original;
            }

            $door->save();
        }

        return response(['status' => 'success']);
    }

    public function getSeries()
    {
        $series = Serie::get();
        return response(['status' => 'success', 'series' => $series]);
    }

    public function getSerie($id)
    {
        $series = Serie::where('id', $id)->with('products')->first();
        return response(['status' => 'success', 'series' => $series]);
    }
}
