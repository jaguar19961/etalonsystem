<?php

namespace App\Http\Controllers;

use App\Cart;
use App\ClientOffer;
use App\Invoice;
use App\ProductColor;
use App\Products;
use App\ProductWindow;
use App\Serie;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OfferClientController extends Controller
{
    public function index()
    {
        return view('site.client.oferte_clienti');
    }

    public function saveOffer(Request $request)
    {
        $offer = new ClientOffer();
        $offer->user_id = auth()->user()->id;
        $offer->client_name = $request->client_name;
        $offer->offer_name = $request->offer_name;
        $offer->seria_id = $request->seria_id;
        $offer->product_id = $request->product_id;
        $offer->color_id = $request->color_id;
        $offer->window_id = $request->window_id;
        $offer->save();
        return response(['status' => 'success']);
    }

    public function getOffers()
    {
        return response(['status' => 'success', 'clients' => ClientOffer::where('user_id', Auth::user()->id)->get()]);
    }

    public function getOffer($id)
    {
        $offer = ClientOffer::where('user_id', Auth::user()->id)->findOrFail($id);
        $type = 1;
        return view('site.client.oferte_client_pdf', compact('offer', 'type'));
    }

    public function getOfferDoor($product_id, $seria_id, $color_id, $window_id, $price, $cart_id)
    {

        $product = ProductColor::findOrFail($color_id);
        $serie = Products::findOrFail($product['product_id']);
        $offer = array(
            'user' => auth()->user(),
            'serie' => $serie->serie,
            'product' => Products::findOrFail($product['product_id']),
            'color' => ProductColor::findOrFail($color_id),
            'window' => $window_id != 'null' ? ProductWindow::findOrFail($window_id) : null,
            'price' => $price,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'cart' => Cart::findOrFail($cart_id),
        );

//        dd($offer);
        $type = 1;
        return view('site.client.oferte_client_pdf_from_order', compact('offer', 'type'));
    }


    public function getOfferDoorForClientSaved($invoice_id)
    {
        $cart = Cart::where('invoice_id', $invoice_id)->get();
        $invoice = Invoice::findOrFail($invoice_id);
        $total = 0;
        foreach ($cart as $item){
            $total += $item->total;
        }
        return view('site.client.oferta_from_cart_client', compact('cart', 'invoice', 'total'));
    }

    public function delOffers($id)
    {
        $offer = ClientOffer::findOrFail($id);
        $offer->delete();

        return response(['status' => 'success']);
    }

    public function offerDoor()
    {

        return view('site.client.offer_client_one_door');
    }

    public function getDoorImage(Request  $request)
    {

        if(!empty($request->window_id)){
            $image = ProductWindow::findOrFail($request->window_id);
        }else{
            $image = ProductColor::findOrFail($request->culoare_id);
        }

        return response(['status' => 'success', 'image' => '/images/catalog/'.$image->image]);
    }
}
