<?php

namespace App\Http\Controllers;

use App\ExtensiiOrizontalePrice;
use App\ExtensiiVerticalePrice;
use App\Serie;
use Illuminate\Http\Request;

class ExtensiiProductController extends Controller
{
    public function index()
    {
        return view('site.operator.extensii_edit_product');
    }

    public function update(Request $request)
    {
        if ($request->get('extensie_name') == 'verticale') {
            $extensie = ExtensiiVerticalePrice::find($request->get('extensie')['id']);
            $extensie->pret = $request->get('extensie')['pret'];
            $extensie->save();
            return response(['status' => 'success'], 200);
        }

        if ($request->get('extensie_name') == 'orizontale') {
            $extensie = ExtensiiOrizontalePrice::find($request->get('extensie')['id']);
            $extensie->pret = $request->get('extensie')['pret'];
            $extensie->save();
            return response(['status' => 'success'], 200);
        }

        return response(['status' => 'error'], 422);
    }

    public function getExtensiiProduct()
    {
        $series = Serie::get();
        return response(['status' => 'success', 'data' => $series]);
    }
}
