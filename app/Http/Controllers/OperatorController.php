<?php

namespace App\Http\Controllers;

use App\Colors;
use App\ExtensiiOrizontalePivot;
use App\ExtensiiOrizontalePrice;
use App\GrindaPrice;
use App\Invoice;
use App\InvoiceGroup;
use App\Molding;
use App\Partener;
use App\ProductColor;
use App\Products;
use App\ProductWindow;
use App\Serie;
use App\TraversaVerticalaPrice;
use App\User;
use App\Window;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Image;
use function GuzzleHttp\Promise\all;

class OperatorController extends Controller
{
    public function contulMeu()
    {
        return view('site.operator.dashboard');
    }

    public function parteneri()
    {
        return view('site.operator.parteneri');
    }

    public function invoice()
    {
        return view('site.operator.invoices');
    }

    public function plinta()
    {
        return view('site.operator.plinta');
    }

    public function minere()
    {
        return view('site.operator.minere');
    }

    public function invoiceManufacture()
    {
        return view('site.operator.invoices-manufacture');
    }

    public function invoiceInProcess()
    {
        return view('site.operator.invoices-in-process');
    }

    public function editProduct()
    {
        $doorsSerie = Serie::get();
        $system_moldings = Molding::get();
        return view('site.operator.edit-product-details', compact('doorsSerie', 'system_moldings'));
    }

    public function config()
    {
        $extensiiorizontale = ExtensiiOrizontalePrice::get();
        $extensiiverticale = ExtensiiOrizontalePrice::get();
        $grinda = GrindaPrice::get();
        $traversaverticala = TraversaVerticalaPrice::get();
        return view('site.operator.config', compact('extensiiorizontale', 'extensiiverticale', 'grinda', 'traversaverticala'));
    }

    public function savePriceForDoors(Request $request)
    {
//        dd($request->hasFile('image'));
//        dd($request->all());
        if ($request->window_id != null) {
            $new = ProductWindow::where('id', $request->window_id)->first();

            if ($request->get('image')) {
                $path = public_path('/images' . '/catalog' . '/' . $new->image);
                File::delete($path);
                $path = public_path('/images' . '/catalog-img-original' . '/' . $new->image_original);
                File::delete($path);

                $image = $request->get('image');
                $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->resize(null, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('images/catalog/') . $name);
                $new->image = $name;

                $name_original = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/catalog-img-original/') . $name_original);
                $new->image_original = $name_original;
            }

            $new->price = $request->price;
            $new->price_matte = $request->price_matte;
            $new->price_crom = $request->price_crom;
            $new->save();
            return response(['status' => 'success']);
        } else {
            $new = ProductColor::where('id', $request->color_id)->first();

            if ($request->get('image')) {
                $path = public_path('/images' . '/catalog' . '/' . $new->image);
                File::delete($path);

                $path = public_path('/images' . '/catalog-img-original' . '/' . $new->image_original);
                File::delete($path);
                $image = $request->get('image');
                $name = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->resize(null, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('images/catalog/') . $name);
                $new->image = $name;

                $name_original = time() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/catalog-img-original/') . $name_original);
                $new->image_original = $name_original;
            }
            $new->price = $request->price;
            $new->price_matte = $request->price_matte;
            $new->price_crom = $request->price_crom;
            $new->save();
            return response(['status' => 'success']);
        }


    }

    public function getDoors($id)
    {
        $doors = Serie::with('products')->where('id', $id)->get();
        return response(['status' => 'success', 'doors' => $doors]);
    }

    public function getPartners()
    {
        $partners = Partener::get();
        return response(['status' => 'success', 'parteneri' => $partners]);
    }

    public function getSeries()
    {
        $series = Serie::get();
        return response(['status' => 'success', 'series' => $series]);
    }

//    extensii------------------------------------------------

    public function saveExtensiiOrizontale(Request $request)
    {
//        dd($request);

        $getSeries = Serie::get();

        $old = ExtensiiOrizontalePivot::get();
        foreach ($old as $da) {
            $da->delete();
        }

        foreach ($getSeries as $key => $series) {
            foreach ($request->all() as $k => $el) {
                foreach ($el as $ki => $ele) {
                    if ($key == $k) {
                        $ext = new ExtensiiOrizontalePivot();
                        $ext->seria_id = $series->id;
                        $ext->extension_id = $ele;
                        $ext->save();
                    }
                }


            }
        }
        return response(['status' => 'success']);
    }

    public function getInvoices($type)
    {
        $invoices = Invoice::where('process', $type)->with('carts')->orderBy('created_at', 'desc')->with('userInfo')->get();

        $total_invoices_tva = 0;
        $total_invoices_partener_tva = 0;
        foreach ($invoices as $key => $total) {
            $total_invoices_tva += $total->total_with_discount;
            $total_invoices_partener_tva += $total->total_with_discount_tva;
        }

        if ($invoices == null) {
            return response(['status' => 'cosgol']);
        } else {
            return response(['status' => 'success', 'invoices' => $invoices, 'total_invoices_tva' => $total_invoices_tva, 'total_invoices_partener_tva' => $total_invoices_partener_tva]);
        }
    }

    public function SendToManufacture($group_id)
    {
        $group = InvoiceGroup::where('id', $group_id)->with('invoicesOrder')->first();
//        dd($group->invoicesOrder);
        foreach ($group->invoicesOrder as $key => $invoice) {
            $inv = Invoice::findOrFail($invoice->id);
            $inv->process = 4;
            $inv->save();
        }
        $group->status = 0;
        $group->to_manufacture = 1;
        $group->save();
        return response(['status' => 'success']);
    }

    public function InvoiceState($process, $invoice_id)
    {
//        verific daca exista un grup de invoice deschis (1) daca este inchis (0)
        $getOldGroup = InvoiceGroup::where('status', 1)->first();

//        dd($getOldGroup->id);

//        daca nu existia nici un grup deschis atunci creiez unul nou daca exista din getoldgroup scot id-ul
//        daca process adica pasul este catre prelucrare factura si este trimis (3) atunci execut procesul descris mai sus
        if ($process == 3) {
            if ($getOldGroup == null) {
                $group = new InvoiceGroup();
                $group->status = 1;
                $group->save();

                $invoices = Invoice::where('id', $invoice_id)->first();
                $invoices->process = $process;
                $invoices->group_id = $group->id;
                $invoices->save();
            } else {
                $invoices = Invoice::where('id', $invoice_id)->first();
                $invoices->process = $process;
                $invoices->group_id = $getOldGroup->id;
                $invoices->save();
            }


        } else {
            $countInvoices = Invoice::where('group_id', $getOldGroup->id)->count();
            if ($countInvoices > 1) {
                $invoices = Invoice::where('id', $invoice_id)->first();
                $invoices->process = $process;
                $invoices->group_id = null;
                $invoices->save();
            } else {
                $invoices = Invoice::where('id', $invoice_id)->first();
                $invoices->process = $process;
                $invoices->group_id = null;
                $invoices->save();
                $delGrpup = InvoiceGroup::findOrFail($getOldGroup->id);
                $delGrpup->delete();
            }

        }


        return response(['status' => 'success']);

    }

    public function deleteDoor($type, $id)
    {
//        daca este 0 atunci se sterge din colectia usa
        if ($type == 0) {
            $door = Products::findOrFail($id);
            $door->delete();
        }

//        daca este 1 atunci se sterge din culoare usa
        if ($type == 1) {
            $door = ProductColor::findOrFail($id);
            $door->delete();
        }

//        daca este de tip 2 atunci se sterge din sticle usa
        if ($type == 2) {
            $door = ProductWindow::findOrFail($id);
            $door->delete();
        }

        return response(['status' => 'success']);
    }

    public function getWindowColor()
    {
        $colors = Colors::get();
        $window = Window::get();
        return response(['status' => 'success', 'colors' => $colors, 'windows' => $window]);
    }

    public function SetOrderDoor(Request $request)
    {
        $allDoors = $request->product;
        $i = 0;
        foreach ($allDoors as $item) {
            $dor = Products::findOrFail($item['id']);
            $dor->position = $i++;
            $dor->save();
        }
        return response(['status' => 'success']);
    }

    public function savePartnersData(Request $request)
    {
        if ($request->id != null) {
            $partner = Partener::findOrFail($request->id);
            $partner->name = $request->name;
            $partner->phone = $request->phone;
            $partner->address = $request->address;
            $partner->discount = $request->discount;
            $partner->discount_minere = $request->discount_minere;
            $partner->bank_details = $request->bank_details;
            $partner->save();
        } else {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|string|email|max:191|unique:users,email',
                'phone' => 'required',
                'address' => 'required',
                'discount' => 'required',
                'discount_minere' => 'required',
                'bank_details' => 'required',
            ]);


            $newUser = new User();
            $newUser->name = $request->name;
            $newUser->email = $request->email;
            $newUser->user_type = 1;
            $newUser->password = '$2y$10$I0AjX.tP0DBXItVA3ELreu/7MrZkQDvBAL3BDivkm1TNOyY/1ODvm';
            $newUser->save();

            $partner = new Partener();
            $partner->name = $request->name;
            $partner->user_id = $newUser->id;
            $partner->email = $request->email;
            $partner->phone = $request->phone;
            $partner->address = $request->address;
            $partner->discount = $request->discount;
            $partner->discount_minere = $request->discount_minere;
            $partner->bank_details = $request->bank_details;
            $partner->save();
        }
        return response(['status' => 'success']);
    }
}
