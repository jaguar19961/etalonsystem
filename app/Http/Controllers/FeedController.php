<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Serie;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FeedController extends Controller
{
    public function export()
    {
        return view('feed');
    }

    public function download()
    {
        $products = Serie::with('products')->get();
        $data = [];
        foreach ($products as $ke => $prod) {
            foreach ($prod->products as $key => $item) {
                foreach ($item['colors'] as $color) {
                    if (count($color['windows']) > 0) {
                        foreach ($color['windows'] as $window) {
                            $data[$prod['name']][] = [
                                'collection' => $prod['name'],
                                'model' => $item['name'],
                                'color_name' => $color['color']['name'] ?? '',
                                'window_name' => $window['window']['name'] ?? '',
                                'product_image' => 'https://order.profildoors.ro/images/catalog/' . $window['image_original'],
                                'price_base' => $window['price'],
                                'price_matte' => $window['price_matte'],
                                'price_crom' => $window['price_crom']
                            ];
                        }
                    } else {
                        $data[$prod['name']][] = [
                            'collection' => $prod['name'],
                            'model' => $item['name'],
                            'color_name' => $color['color']['name'] ?? '',
                            'window_name' => '',
                            'product_image' => 'https://order.profildoors.ro/images/catalog/' . $color['image_original'],
                            'price_base' => $color['price'],
                            'price_matte' => $color['price_matte'],
                            'price_crom' => $color['price_crom'],
                        ];
                    }
                }

            }
        }
        return Excel::download(new ProductExport($data), 'feed.xlsx');
    }
}
