<?php

namespace App\Http\Controllers;

use App\Colors;
use App\Minere;
use App\ProductColor;
use App\Products;
use App\ProductTag;
use App\ProductWindow;
use App\Serie;
use App\Tag;
use App\Window;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    protected $model;

    public function __construct(Products $products)
    {
        $this->model = $products;
    }

    public function get(Request $request, $serie_id = null)
    {
        $dors = Products::with('colors')->where(function ($query) use ($request, $serie_id) {
            if ($serie_id != null) {
                $query->where('serie_id', $serie_id);
            }

//            $price = json_decode($request->price);
//            $query->whereBetween('price', $price);

            if ($request->color_id != null) {
                $color_id = $request->color_id;
                $query->whereHas('colors', function ($query) use ($color_id) {
                    $query->where('color_id', $color_id);
                });
            }

//            filtrare dupa insertii
            if ($request->window_id != null) {
                $window_id = $request->window_id;
                $query->whereHas('windows', function ($query) use ($window_id) {
                    $query->where('window_id', $window_id);
                });
            }
//            filtrare dupa style
//
//            if ($request->door_type != null) {
//                dd($request->door_type);
//                $query->whereHas('tags', function ($q) use ($styles))
//                $query->where('type_id', $request->door_type);
//            }

        });
        if (!is_null($request->styles)) {
            $styles = json_decode($request->styles, true);
            $styles = array_filter($styles);
            if (!empty($styles)) {
                $doors = ProductTag::whereIn('tag_id', $styles)->pluck('product_id')->toArray();
                $ids = array_unique($doors);
                $dors = $dors->whereIn('id', $ids);
            }
        }
        $dors = $dors->paginate(8);
        return response($dors);
    }

    public function getSeries()
    {
        $series = Serie::with('product')->get();
        return response($series);
    }

    public function getSerie($id)
    {
        $serie = Serie::findOrFail($id);
        return response($serie, 200);
    }

    public function getProduct($id)
    {
        $product = Products::findOrFail($id);
        return response(['status' => 200, 'data' => $product]);
    }

    public function getFeronerie()
    {
        $manere = Minere::get();
        return response(['status' => 200, 'data' => $manere]);
    }

    public function getSimilar($serie_id)
    {
        $products = Products::where('serie_id', $serie_id)->inRandomOrder()->take(10)->get();
        return response(['status' => 200, 'data' => $products]);
    }

    public function getColors($serie_id = null)
    {
        if ($serie_id == null) {
            $groupcolors = Colors::get();
        } else {
            $showProductSeries = Products::where('serie_id', $serie_id)->get();
            $cat = $showProductSeries->pluck('id')->toArray();
            $groupcolors = Colors::whereHas('products', function ($query) use ($cat) {
                $query->whereIn('products.id', $cat);
            })->distinct()->orderBy('type_id')->get();
        }


        return response(['status' => 200, 'data' => $groupcolors]);
    }

    public function getWindows($serie_id = null)
    {
        if ($serie_id == null) {
            $groupwindows = Window::get();
        } else {
            $showProductSeries = Products::where('serie_id', $serie_id)->get();
            $cat = $showProductSeries->pluck('id')->toArray();
            $groupwindows = Window::whereHas('products', function ($query) use ($cat) {
                $query->whereIn('products.id', $cat);
            })->distinct()->get();
        }


        return response(['status' => 200, 'data' => $groupwindows]);
    }

    public function getTags()
    {
        $tags = Tag::get();
        return response(['status' => 200, 'data' => $tags], 200);
    }

    public function catalog(Request $request)
    {
        $dors = Products::with('colors');
        if (!empty($request->get('series'))) {
            $series = array_map('intval', explode(',', $request->get('series')));
            $dors = $dors->whereIn('serie_id', $series);
        }

        //filters####################
        $disponible_products = array_map('intval', $dors->pluck('id')->toArray());

        $prod_colors = ProductColor::whereIn('product_id', $disponible_products)->pluck('color_id')->toArray();
        $selected_colors = array_map('intval', array_unique($prod_colors));
        $colors = Colors::whereIn('id', $selected_colors)->get();

        $prod_window = ProductWindow::whereIn('product_id', $disponible_products)->pluck('window_id')->toArray();
        $selected_window = array_map('intval', array_unique($prod_window));
        $windows = Window::whereIn('id', $selected_window)->get();

        if (!empty($request->get('colors'))) {
            $color = array_map('intval', explode(',', $request->get('colors')));
            $dors = $dors->whereHas('colors', function ($query) use ($color) {
                $query->whereIn('color_id', $color);
            });
        }

        if (!empty($request->get('insertii'))) {
            $window = array_map('intval', explode(',', $request->get('insertii')));
            $dors = $dors->whereHas('windows', function ($query) use ($window) {
                $query->whereIn('window_id', $window);
            });
        }

        if (!empty($request->get('tags'))) {
            $tags = array_map('intval', explode(',', $request->get('tags')));
            $dors = $dors->whereHas('tags', function ($query) use ($tags) {
                $query->whereIn('tag_id', $tags);
            });
        }

        $dors = $dors->paginate(8);
        $data = [
            'doors' => $dors,
            'colors' => $colors,
            'windows' => $windows,
        ];
        return response($data);
    }

}
