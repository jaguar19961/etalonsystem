<?php

namespace App\Http\Middleware;

use Closure;

class isStore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && $request->user()->user_type == 3 ){
            return $next($request);
        }
        return redirect()->guest('/');
    }
}
