<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtensiiVerticalePivot extends Model
{
    protected $table = 'extensii_verticale_pivots';

    protected $with = ['price_extension_vertical'];

    public function price_extension_vertical()
    {
        return $this->hasMany(ExtensiiVerticalePrice::class, 'grup', 'extension_id');
    }

    public function products(){
        return $this->hasMany(Products::class, 'serie_id', 'seria_id');
    }
}
