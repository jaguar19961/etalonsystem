<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Window extends Model
{
    use SoftDeletes;
    protected $table = 'windows';


    public function products(){
        return $this->belongsToMany('App\Products', 'product_windows', 'window_id', 'product_id');
    }
}
