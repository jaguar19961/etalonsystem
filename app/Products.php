<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Products extends Model
{
    protected $table = 'products';

    protected $fillable = ['id', 'order', 'image', 'price', 'serie_id'];

    protected $with = ['windows', 'colors', 'serie', 'tags'];

    protected $appends = ['tags_array'];

    public function serie()
    {
        return $this->hasOne(Serie::class, 'id', 'serie_id');
    }

    public function colors()
    {
        return $this->hasMany(ProductColor::class, 'product_id', 'id');
    }

    public function color()
    {
        return $this->hasOne(ProductColor::class, 'product_id', 'id');
    }

    public function furnitures()
    {
        return $this->hasMany(ProductFurniture::class, 'product_id', 'id');
    }

    public function style()
    {
        return $this->hasOne(Style::class, 'style_id', 'id');
    }

    public function windows()
    {
        return $this->hasManyThrough(
            'App\ProductWindow',
            'App\ProductColor',
            'product_id',
            'color_id'
        );
    }

//    public function getPriceAttribute($price)
//    {
//        return round($price * 0.05 + $price);
//    }

    public function tags()
    {
        return $this->hasMany(ProductTag::class, 'product_id', 'id');
    }

    public function getTagsArrayAttribute()
    {
        return $this->tags->pluck('tag_id')->toArray();
    }
}
