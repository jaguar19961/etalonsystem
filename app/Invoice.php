<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Invoice extends Model
{
    use SoftDeletes;

    protected $table = 'invoices';
    protected $appends = ['name_sum', 'invoice_sum', 'combine_elements', 'invoice_specification'];
    protected $with = ['userDetails'];

    public function userInfo()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function userDetails()
    {
        return $this->hasOne(Partener::class, 'user_id', 'user_id');
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'invoice_id', 'id');
    }

    public function cartFiltered()
    {
        return $this->hasMany(Cart::class, 'invoice_id', 'id')->select('specification');
    }

    public function GetInvoiceSpecificationAttribute(){
        $collection = $this->cartFiltered()->get('specification');

        $names = $collection->map(function($item, $key) {
            return $item->specification;
        });

        return $names;


    }

    public function GetInvoiceSumAttribute(){
        $collection = $this->carts()->get()->groupBy('invoice_id');
        $res = array();

        foreach ($collection as $key=> $a){
            $res = $a->sum('total');
        }

        return $res;

    }

    public function GetNameSumAttribute(){

        $collection = $this->carts()->get()->groupBy('pentru_cine');

        $names = $collection->map(function($item, $key) {
           $names['items'] = $item;
           $names['total'] = $item->sum('total');
           return $names;
        });

        return $names;


    }

    public function GetCombineElementsAttribute(){

        $collection = $this->carts()->get()->groupBy('pentru_cine');

        $names = $collection->map(function($item, $key) {
            $names['items'] = $item;
            $names['total'] = $item->sum('total');
            return $names;
        });

//        dd($names);
        return $names;


    }


}
