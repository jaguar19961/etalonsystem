<?php

namespace App\Console\Commands;

use App\ProductColor;
use App\Products;
use App\ProductWindow;
use Illuminate\Console\Command;

class AddProccentPriceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:price-procent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $products;
    protected $productColor;
    protected $productWindow;
    protected $procent = 0.05;

    public function __construct(Products $products, ProductColor $productColor, ProductWindow $productWindow)
    {
        $this->products = $products;
        $this->productColor = $productColor;
        $this->productWindow = $productWindow;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $this->info(collect($this->products->get()));
//        try {
        $this->productsProcent($this->products);
        $this->productsColorProcent($this->productColor);
        $this->productsWindowProcent($this->productWindow);
//        return response('success');
//        } catch (\Throwable $e) {
//            return response($e);
//        }
    }

    public function productsProcent($products)
    {
        $this->info('product start');
        foreach ($products->get() as $key => $product) {
            $product->update([
                'price' => round($product->price * $this->procent + $product->price)
            ]);
            $this->info('product start' . $key);
        }
    }

    public function productsColorProcent($products)
    {
        $this->info('product color start');
        foreach ($products->get() as $key => $product) {
            $product->update([
                'price' => round($product->price * $this->procent + $product->price),
                'price_matte' => round($product->price_matte * $this->procent + $product->price_matte),
                'price_crom' => round($product->price_crom * $this->procent + $product->price_crom)
            ]);
            $this->info('product color start'. $key);
        }
    }

    public function productsWindowProcent($products)
    {
        $this->info('product window start');
        foreach ($products->get() as $key => $product) {
            $product->update([
                'price' => round($product->price * $this->procent + $product->price),
                'price_matte' => round($product->price_matte * $this->procent + $product->price_matte),
                'price_crom' => round($product->price_crom * $this->procent + $product->price_crom)
            ]);
            $this->info('product window start' . $key);
        }
    }
}
