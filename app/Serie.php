<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    protected $table = 'series';

    protected $with = ['extensiiverticales', 'extensiiorizontale'];

    protected $fillable = ['category_id', 'name', 'images', 'description', 'furniture', 'grosime_canat', 'traversa', 'grinda'];

    public function product()
    {
        return $this->hasOne(Products::class, 'serie_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Products::class, 'serie_id', 'id')->orderBy('position', 'asc');
    }

    public function furnitur()
    {
        return $this->hasMany(SerieFurniture::class, 'serie_id', 'id');
    }

//    ### TRAVERSE VERTICALE LEGATURA ###
    public function traversa_verticala()
    {
        return $this->hasOne(TraversaVerticalaPivot::class, 'seria_id', 'id');
    }

    public function grinda()
    {
        return $this->hasOne(GrindaPivot::class, 'seria_id', 'id');
    }

//    extensii=---------preturi---------------
    public function extensiiverticales()
    {
        return $this->hasMany(ExtensiiVerticalePivot::class, 'seria_id', 'id');
    }

    public function extensiiorizontale()
    {
        return $this->hasMany(ExtensiiOrizontalePivot::class, 'seria_id', 'id');
    }

}
