<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrindaPivot extends Model
{
    protected $table = 'grinda_pivots';

    protected $with = ['infoPriceSimplu'];


    public function infoPriceSimplu()
    {
        return $this->hasMany(GrindaPrice::class, 'grup', 'grinda_id')->where('tip', 0);
    }
}
