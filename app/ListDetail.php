<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListDetail extends Model
{
    protected $with = ['colectii'];

    public function colectii(){
        return $this->hasMany(ListDetailCollection::class, 'list_detail_id', 'colectii');
    }
}
