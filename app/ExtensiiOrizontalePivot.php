<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtensiiOrizontalePivot extends Model
{
    protected $table = 'extensii_orizontale_pivots';

    protected $with = ['price_extension_orizontal'];

    public function price_extension_orizontal()
    {
        return $this->hasMany(ExtensiiOrizontalePrice::class, 'grup', 'extension_id')->where('tip', 0);
    }

    public function products(){
        return $this->hasMany(Products::class, 'serie_id', 'seria_id');
    }
}
