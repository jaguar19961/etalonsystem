<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListType extends Model
{

    public function listDetails(){
        return $this->hasMany(ListDetail::class, 'list_type_id', 'id');
    }
}
