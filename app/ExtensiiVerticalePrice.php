<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtensiiVerticalePrice extends Model
{
    protected $table = 'extensii_verticale_prices';
}
