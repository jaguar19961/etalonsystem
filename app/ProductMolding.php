<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMolding extends Model
{
    protected $table = 'product_moldings';

    protected $with = ['molding'];

    public function molding()
    {
        return $this->hasOne(Molding::class, 'id', 'molding_id');
    }
}
