<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListDetailCollection extends Model
{
    protected $with = ['serie'];

    public function serie()
    {
        return $this->hasOne(Serie::class, 'id', 'collection_id');
    }
}
