<?php

namespace App\Exports;

use App\Products;
use App\Serie;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductExport implements FromCollection, WithHeadings
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect($this->data);
    }

//    public function map($items): array
//    {
////        dd($items);
//        return [
//            $items->collection,
////            $items['model'],
////            $items['color_name'],
////            $items['window_name'],
////            $items['product_image'],
////            $items['price_base'],
////            $items['price_matte'],
////            $items['price_crom'],
//        ];
//    }

    public function headings(): array
    {
        return [
            'Colectia',
            'Model',
            'Culoare',
            'Sticla',
            'Imagine',
            'Pret Standart',
            'Pret Matte',
            'Pret Crom',
        ];
    }
}
