<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductWindow extends Model
{
    protected $table = 'product_windows';

    protected $model_name = 'App\\Models\\ProductWindow::class';

    protected $with = ['window', 'moldings'];

    protected $fillable = ['price', 'price_matte', 'price_crom', 'product_id'];

    public function window()
    {
        return $this->hasOne(Window::class, 'id', 'window_id');
    }

    public function moldings()
    {
        return $this->hasMany(ProductMolding::class, 'product_id', 'id')
            ->where('model_name', $this->model_name);
    }

//    public function getPriceAttribute($price)
//    {
//        return round($price * 0.05 + $price);
//    }
//
//    public function getPriceMatteAttribute($price)
//    {
//        return round($price * 0.05 + $price);
//    }
//
//    public function getPriceCromAttribute($price)
//    {
//        return round($price * 0.05 + $price);
//    }
}
