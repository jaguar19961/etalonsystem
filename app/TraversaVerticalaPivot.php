<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraversaVerticalaPivot extends Model
{
    protected $table = 'traversa_verticala_pivots';

    protected $with = ['infoPriceSimplu'];

    public function infoPriceSimplu()
    {
        return $this->hasMany(TraversaVerticalaPrice::class, 'grup', 'traversa_id');
    }
}
