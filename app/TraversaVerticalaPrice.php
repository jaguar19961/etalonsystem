<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraversaVerticalaPrice extends Model
{
    protected $table = 'traversa_verticala_prices';
}
