<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceGroup extends Model
{
    use SoftDeletes;

    protected $table = 'invoice_groups';
    protected $appends = ['group_sum', 'group_specification', 'combine_product'];

    protected $with = ['invoicesOrder.cartFiltered'];

    public function invoicesOrder()
    {
        return $this->hasMany(Invoice::class, 'group_id', 'id');
    }

    public function GetGroupSumAttribute(){
        $collection = $this->invoicesOrder()->get();

        $total = 0;
        foreach ($collection as $key => $da){
//           $total += $da->invoice_sum;
           $total += $da->total_with_discount;
        }

        return $total;
    }

    public function GetGroupSpecificationAttribute(){
        $collection = $this->invoicesOrder()->get();

        $names = $collection->map(function($item, $key) {
            $da = $item['invoice_specification'];
            $spec = $da->map(function ($it, $k) {
                return $it;
            });
            return $spec;
        });

        return $names;
    }

    public function GetCombineProductAttribute(){
        $products = $this->GetGroupSpecificationAttribute();

        $names = $products->map(function($item, $key) {
            return $item;
        });

        return $products;
    }
}
