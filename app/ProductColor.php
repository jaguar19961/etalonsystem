<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model
{
    protected $table = 'product_colors';

    protected $model_name = 'App\\Models\\ProductColor::class';

    protected $with = ['windows', 'color', 'moldings'];

    protected $fillable = ['price', 'price_matte', 'price_crom', 'color_id'];

//    protected $attributes = ['price'];

    public function color()
    {
        return $this->hasOne(Colors::class, 'id', 'color_id');
    }

    public function windows()
    {
        return $this->hasMany(ProductWindow::class, 'color_id', 'id');
    }

    public function moldings()
    {
        return $this->hasMany(ProductMolding::class, 'product_id', 'id')
            ->where('model_name', $this->model_name);
    }

//    public function getPriceAttribute($price)
//    {
//        return round($price * 0.05 + $price);
//    }
//
//    public function getPriceMatteAttribute($price)
//    {
//        return round($price * 0.05 + $price);
//    }
//
//    public function getPriceCromAttribute($price)
//    {
//        return round($price * 0.05 + $price);
//    }
}
