<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoorType extends Model
{
    protected $table = 'door_types';
}
