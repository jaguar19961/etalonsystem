<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientOffer extends Model
{
    protected $table = 'client_offers';

    protected $with = ['product', 'serie', 'window', 'user', 'color'];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function product()
    {
        return $this->hasOne(Products::class, 'id', 'product_id');
    }

    public function serie()
    {
        return $this->hasOne(Serie::class, 'id', 'seria_id');
    }

    public function color()
    {
        return $this->hasOne(ProductColor::class, 'id', 'color_id');
    }

    public function window()
    {
        return $this->hasOne(ProductWindow::class, 'id', 'window_id');
    }
}
